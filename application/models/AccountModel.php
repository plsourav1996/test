<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AccountModel extends CI_Model {

	function __construct()
    {
    	parent::__construct();      
    }  

function get_data_where($where,$table){
    $this->db->where($where);
    $r=$this->db->get($table);
    return $r->result();
    }
    function get_data_where2($where1,$where2,$table){
        $this->db->where($where1);
        $this->db->where($where2);
        $r=$this->db->get($table);
        return $r->result();
        }
		

         /////////////////////////////


        public function order_details($orderId){
        $this->db->select('orderdetails.quantityOrdered,orderdetails.priceEach,orderdetails.quantityOrdered,
                          products.productLine,products.productName');
        $this->db->from('orderdetails');
        $this->db->join('products','orderdetails.productCode = products.productCode');
        $this->db->where('orderdetails.orderNumber',$orderId);
        $query=$this->db->get();
        $data=$query->result();
        return $data;
    }


}
