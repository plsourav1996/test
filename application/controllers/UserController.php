<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('AccountModel');
    }
	
	public function index(){
		$this->load->view('login');
	}
	
	public function cklogin(){
			
		if($this->input->post('submit')!=''){
			$number = $this->input->post('number');
			$check_cust_login =  $this->AccountModel->get_data_where(array('customerNumber'=>$number),'customers');
			if(!empty($check_cust_login)){
				$data['msg'] = 'Welcome Customer';
				$this->defaultPage($data);
			}
			
			if(empty($check_cust_login)){
				$check_admin_login =  $this->AccountModel->get_data_where(array('employeeNumber'=>$number),'employees');
				if(!empty($check_admin_login)){
					$data['msg'] = 'Welcome Admin';
					$this->defaultPage($data);
				}
				else{
					$this->session->set_flashdata('error_msg',"invalid Number");
					redirect(base_url(''));
				}
			}
			
			
			
		}
	}
	
	
	public function defaultPage($data){
		$this->load->view('defaultPage',$data);
	}
	
	public function check($data){
		echo "<pre>";
		print_r($data);
		die();
	
	}
	
	

}
