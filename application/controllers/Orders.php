<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . 'libraries/REST_Controller.php';
     
class Orders extends REST_Controller {
    
    public function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
       parent::__construct();
       $this->load->model('AccountModel');
       $this->token = '4atilefN0Z'; 
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($orderid){
        
        if($orderid !=''){
        	$order =  $this->AccountModel->get_data_where(array('orderNumber'=>$orderid),'orders');
        	if(!empty($order)){
        		foreach ($order as $key => $value) {
        			$orderNumber = $value->orderNumber;
    				$orderDate = $value->orderDate;
    				$status = $value->status;
    				$customerNumber = $value->customerNumber;

        		}

        		$fetchOrderDetails = $this->AccountModel->order_details($orderid);
        		$customerDetails = $this->AccountModel->get_data_where(array('customerNumber'=>$customerNumber),'customers');

        		foreach ($customerDetails as$_value) {
        			$first_name = $_value->contactFirstName;
    				$last_name = $_value->contactLastName;
    				$phone = $_value->phone;
    				$country_code = $_value->country;
        		}

        		$order = array(
        			'orderNumber'=>$orderNumber,
        			'orderDate'=>$orderDate,
        			'status'=>$status,

        		);
                 $bill_amount = 0 ;
        		foreach($fetchOrderDetails as $val) {
	        			$order['orderDetails'][] = array(
	        				'quantityOrdered'=>$val->quantityOrdered,
	        				'priceEach'=>$val->priceEach,
	        				'productLine'=>$val->productLine,
	        				'productName'=>$val->productName,
	        				'line_total'=>($val->quantityOrdered * $val->priceEach),
                             $bill_amount+= $val->quantityOrdered * $val->priceEach,
	        			);
	        	}
                $order['bill_amount'] =$bill_amount;
	        	$order['customer'] = array(
        			'first_name'=>$first_name,
        			'last_name'=>$last_name,
        			'phone'=>$phone,
        			'country_code'=>$country_code,
        		);
        		if(!empty($order)){
        			echo $this->response(array("success"=>1,$order),200);
        		}else{
        			echo $this->response(array("success"=>0),400);
        		}
        }
	}
}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
      
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        
    }
    	
}