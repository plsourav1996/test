<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Admin</title>
        <link href="<?php echo base_url('public') ?>/dist/css/styles.css" rel="stylesheet" />
       <!-- <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url('public/scripts/jquery-3.3.1.min.js') ?>" crossorigin="anonymous"></script>
<link rel="stylesheet" crossorigin="anonymous" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" crossorigin="anonymous" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"/>


   </head>
	<div class="loader"></div>
<style>
.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(<?php echo base_url ('public/img/loding.gif') ?>) 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>
<script type="text/javascript">
  $(window).on('load', function(){ 
      $(".loader").fadeOut("slow");
  });
</script>

	<script>
var myVar = setInterval(myTimer ,1000);
function myTimer() {
  var d = new Date();
  document.getElementById("date_time_view").innerHTML = d.toDateString() + ", "  + d.toLocaleTimeString()  ;
}
</script>
<?php 
	if(empty($this->session->admin)){
		   redirect('');
	}
?>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="<?php echo base_url('dashboard')?>"> lyfeSolve</a>
			<button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#">
			<i class="fas fa-bars"></i>
			</button><!-- Navbar Search-->
			<h5 style="color:white;margin: auto;" id="date_time_view"> </h5>
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <!--
				<div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
				-->
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" 
						  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-user fa-fw"></i>
						</a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo base_url('/logout') ?>"  
								onclick="return confirm('Are you sure you want to Logout?');">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Super admin</div>
                                
                                <a class="nav-link" href="<?php echo base_url('dashboard') ?>">
                                    <div class="sb-nav-link-icon">
                                        <i class="fas fa-tachometer-alt"></i>
                                    </div>
                                    Dashboard
                                </a>
                      
								
									
										<a class="nav-link" href="<?php echo base_url('formData') ?>">
                                    <div class="sb-nav-link-icon">
                                      <i class="fas fa-columns"></i> 
                                    </div>
											Form Data
                                  </a>


                                    <a class="nav-link" href="<?php echo base_url('agentFormData') ?>">
                                    <div class="sb-nav-link-icon">
                                      <i class="fas fa-columns"></i> 
                                    </div>
                                           Agent Form Data
                                  </a>


                                    <a class="nav-link" href="<?php echo base_url('agentQuesSet') ?>">
                                    <div class="sb-nav-link-icon">
                                      <i class="fas fa-columns"></i> 
                                    </div>
                                         Agent Questions Set
                                  </a>
								  

                                    <a class="nav-link" href="<?php echo base_url('AgentAnswerSheet') ?>">
                                    <div class="sb-nav-link-icon">
                                      <i class="fas fa-columns"></i> 
                                    </div>
                                         Agent Answer Sheet
                                  </a>
                                
									
									</div>
                                 
                                 
                                
                                 
                                 
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as: Admin</div>
                      
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                