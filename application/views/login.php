 <?php $this->load->view('include/loginHeader'); ?>
            <div id="layoutAuthentication_content">


        
         <?Php if($this->session->flashdata('error_msg')!=""){?>
                <div class="alert alert-danger">
                    <?php echo  $this->session->flashdata('error_msg') ; ?>
                </div>
        <?php } ?>


                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Login</h3></div>
                                    <div class="card-body">
                                        <form action="<?php echo base_url('cklogin');?>" method="post" >
                                            <div class="form-group"><label class="small mb-1" for="inputEmailAddress">Number</label>
                                                <input class="form-control py-4" id="" type="number" 
                                                placeholder="Enter your Number" min="1" 
                                                  name="number" value=""/>
                                            </div>
                                            
                                            
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                               
                                                <input type="submit" class="btn btn-primary"  name="submit" value="Login" >
                                            </div>
                                                
                                                
                                                 
                                                 
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
           <?php $this->load->view('include/loginFooter'); ?>